<?php

namespace Drupal\Tests\trash\Kernel;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\workspaces\Kernel\WorkspaceTestTrait;
use Drupal\workspaces\Entity\Workspace;

/**
 * Tests Trash integration with Workspaces.
 *
 * @group trash
 */
class TrashWorkspacesTest extends TrashKernelTestBase {

  use WorkspaceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'workspaces',
  ];

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->workspaceManager = \Drupal::service('workspaces.manager');

    $this->installSchema('workspaces', ['workspace_association']);
    $this->installEntitySchema('workspace');

    $this->workspaces['stage'] = Workspace::create(['id' => 'stage', 'label' => 'Stage']);
    $this->workspaces['stage']->save();

    $permissions = array_intersect([
      'administer nodes',
      'create workspace',
      'edit any workspace',
      'view any workspace',
    ], array_keys($this->container->get('user.permissions')->getPermissions()));
    $this->setCurrentUser($this->createUser($permissions));
  }

  /**
   * Test trashing entities in a workspace.
   */
  public function testDeletion(): void {
    $node = $this->createNode(['type' => 'article']);
    $node->save();

    // Activate a workspace and delete the node.
    $this->switchToWorkspace('stage');
    $node->delete();

    // Check loading the deleted node in a workspace.
    $storage = $this->entityTypeManager->getStorage('node');
    $this->assertNull($storage->load($node->id()));
    $this->assertNull($storage->loadRevision($node->getRevisionId()));
  }

}
